import {createStore} from 'vuex'

export default createStore({
    state: {
        url: "https://reqres.in/",
    },
    getters: {
        getUrl(state) {
            return state.url;
        }

    },
    mutations: {},
    actions: {},
    modules: {}
})
