import { createRouter, createWebHashHistory } from 'vue-router'
// import Login from '../views/login.vue'
import Register from '../views/register.vue'
import About from '../views/About.vue'
import  Home from '../views/auth/home'
import  Account from '../views/auth/account.vue'
import Users from "@/views/auth/users";
import EditUser from "@/views/auth/editUser";
import Login from "@/views/login";

const routes = [


  {

    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Register',
   component: Register,
  },
  {
    path: '/about',
    name: 'About',
    component:About
  },

  /**
   * authenticated pages
   */
  {
    path: '/home',
    name: 'Home',
    meta: { requiresAdminAuth: true },
    component: Home
  },
  {
    path: '/account',
    name: 'Account',
    component: Account,
    meta: { requiresAdminAuth: true }

  },
  {
    path: '/users',
    name: 'Users',
    component: Users,
    meta: { requiresAdminAuth: true }

  },
  {
    path: '/edit-user/:id',
    name: 'EditUser',
    component: EditUser,
    meta: { requiresAdminAuth: true }

  },

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  const admin_token = localStorage.getItem('token');
  if (to.matched.some((record) => record.meta.requiresAdminAuth)) {
    if (admin_token) {
      next();
    } else {
      next({path: '/login'});
    }
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  const admin_token = localStorage.getItem('token');
  if (to.matched.some((record) => record.meta.requiresAdminGuest)) {
    if (admin_token) {
      next({path: '/'});
    } else {
      next();
    }
  } else {
    next();
  }

});

export default router
